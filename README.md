# Home Automation

<img src="./pics/logoha.png" alt="drawing" width="250"/>

[Home Assistant](https://www.home-assistant.io/)

Para a instalação do home assitante, existem muitas maneiras de se fazer, eu particulamente gosto do HassIO (Sistema operacional dedicado ao HA) em uma VM dentro do meu servidor pessoal. Dessa maneira tenho um ambiente fechado com todas as integrações que uso nesse projeto dentro de um único local e dessa maneira posso fazer um backup rápido e seguro.

[Metodos de Instalação](https://www.home-assistant.io/installation/)

Este projeto Home Assistant é open source e muito bem documentado sendo o maior projeto de automação residencial e com uma comunidade extremamente envolvida. O Home Assistant tem integração com práticamente tudo que existe no mercado.

Com relação ao código aqui proposto é somente a pasta de configuração do meu Home Assistant (HA) que servirá como base para que vc possa fazer o seu.

![Config Dir](./pics/diretorios.png)

Cada casa é diferente com dispositivos diferentes então tem que estudar e aqui esta um material bom para você pesquisar.

# Meu Dashboard

O arquivo [raw.yaml](/lovelace/raw.yaml) dentro da pasta lovelace contem yaml de toda a configuração visual do meu painel, mas não funcionará para você, pois vc não tem os sensores que eu tenho nomeados do jeito que eu nomeei. Logo, ainda serve para saber como eu fiz para cada botão ficar do jeito que ficou.

Algumas imagens rápidas, pois eu tenho mais de 40 telas.

<img src="./pics/frontal.jpg" alt="drawing" width="220"/>
 <img src="./pics/floorplan.jpg" alt="drawing" width="220"/> <img src="./pics/casal.jpg" alt="drawing" width="220"/> <img src="./pics/sala.jpg" alt="drawing" width="220"/>
 <img src="./pics/informacoes.jpg" alt="drawing" width="220"/> <img src="./pics/infra.jpg" alt="drawing" width="220"/>

## Addons

Addons são containers rodando para aumentar os serviços envolvidos em torno do HA usando containers.

Os que eu utilizo
![addons](./pics/addon.png)

## Hacs
Também recomendo a instalação do HACS que aumenta ainda mais as integrações e os plugins com cards mais legais.

<https://hacs.xyz/>

Intragrações que uso no para melhorar o visual

![frontend](./pics/frontend.png)

Vale muito a pena estudar esse plugin [Custom-Cards](https://github.com/custom-cards/button-card) pois é com ele que eu fiz grande parte da mudança que você viu no visual dos botões.

Integrações com o hacs que utilizo que ainda não são suportadas direto pelo projeto.

![hacsintegration](./pics/integrations.png)

## Integrações nativas do HA

Dentro do HA ainda é possível fazer integrações que já estão preparadas para utilizar. Algumas das que utilizo

![haintegration](./pics/haintegrations.png)


## Dicas

- Tenha uma boa rede em casa para suportar muitos dispositivos. Boa rede não significa velocidade, mas estabilidade. Um bom roteador e um bom acess point vai te dar menos dor de cabeça. Eu uso um sistema da Ubiquiti, pois tenho mais de 200 dispositivos.

- Se for usar um raspberry pi evite o uso de microsd, existe meios de colocar um SSD. microsd corrompe demais, e quando falamos de casa vc não vai querer seu sistema dando problema toda hora. Outro detalhe é que vc pode utilizar um powerbank para alimentar o raspberry como um nobreak para ele.

- Se for integrar camera, existem addons que suportam a gravação dentro do próprio HA, porém um raspberry pi não será recomendado.

- Siga inumeros canais e fique atento as novidades do mercado. Toda hora sai algo novo ou um jeito mais fácil de fazer, por isso essas configurações são somente um guia.

- Procure dividir a rede com segurança e expor o menos possível e utilize a cloudflare para evitar algum tipo de ataque. Procure também utilizar uma vpn para acessar sua residência.

- Antes de comprar algo para a sua casa sempre busque nas comunidades se aquilo é facilmente integrável e se vc irá conseguir fazer.

- Eu fiz a minha integração de voz toda utilizando Alexa e não me arrependo.

- Câmeras possuem sensor de movimento que podem ser utilizado para suas automações.

.... poderia escrever inumeras dicas aqui, mas como o tempo elas se tornam obsoletas pois tudo muda demais nesse cenário.

**Esse projeto serve como base de estudo para quem esta começando para consulta de código. Por isso deixei muita coisa documentada e escrita no código.** 
